#!/usr/bin/env python

import numpy as np
import os, serial,time,socket,sys,json,logging,requests,getopt
from meas_data import meas_data
# import the server implementation
# import ADC
pathname = os.path.dirname(sys.argv[0])        
abspath=os.path.abspath(pathname)

configfile=abspath+"/config.json"
cf=open(configfile,"r")
log_conf=json.load(cf)
cf.close()

channel_names=[]
channel_info={}

parameter={"device":socket.gethostname(),"mean_count":5,"ring_length":10,"wait":0.5,"sigma":2,"check_last":5,"gpg_keyid":""}
for n in parameter:
  if n in log_conf:
    parameter[n]=log_conf[n]


# declare ve mppt
bve=False
if "vedirect" in log_conf:
  from vedirect import vedirect
  bmqtt=False
  if "mqtt" in log_conf:
    bmqtt=True
    lcmq=log_conf['mqtt']
    mbroker=""
    if 'broker' in lcmq:
      mbroker=lcmq['broker']
    else:
      bmqtt=False
    mport=1883
    if 'port' in lcmq:
      mport=lcmq['port']  

  if "sqlserver" in log_conf:
    hostname="localhost"
    port=8080

  bve=True
  if "enable" in log_conf['vedirect']:
    if log_conf['vedirect']['enable'] == 0:
      bve=False
  if "port" in log_conf['vedirect']:
    bve_port=log_conf['vedirect']['port']
  else:
    bve_port='/dev/serial/by-id/usb-VictronEnergy_BV_VE_Direct_cable_VE1SSBVT-if00-port0'
  print(bve_port)
  try:
    ve=vedirect(bve_port,60)
  except:
    bve=False
  else:
    tsl_sigma=parameter['sigma']
    for n in parameter:
      if n in log_conf['vedirect']:
        parameter[n]=log_conf['vedirect'][n]
    if "sigma" in log_conf['vedirect']:
      tsl_sigma=int(log_conf['vedirect']['sigma'])
      if tsl_sigma < 1:
        tsl_sigma=1
    varnames={"volt_bat":"V","volt_arr":"VPV","amp_bat":"I","watt":"PPV","days":"HSDS"}
    varnamess={"volt_bat":"voltage","volt_arr":"voltage","amp_bat":"current","watt":"power","days":"days"}
    mult={"volt_bat":1000,"volt_arr":1000,"amp_bat":1,"watt":1,"days":1}
    sensor={"volt_bat":"VE_Battery","volt_arr":"VE_Array","amp_bat":"VE_Battery","watt":"VE_Battery","days":"VE_Array"}
    for i in varnames:
      channel_info[i]={"meas_data": meas_data(var_name=varnamess[i],ring_length=parameter['ring_length'],device=parameter['device'],sensor=sensor[i],i2c=0,store_dir="/home/pi/log/",sigma=tsl_sigma,digits=3,multiplicator=mult[i],check_last=parameter['check_last'])}
      if bmqtt and ("mqtt" in log_conf['vedirect']):
        channel_info[i]['meas_data'].set_mqtt(broker=mbroker,port=mport)
      if "sqlserver" in log_conf:
        channel_info[i]['meas_data'].set_sql(host=hostname,port=port)


if bve:
  while True:
  
    try:
      vedata=ve.read_data_single()
    except:
      print("could not read VE")
    else:
      for i in ("volt_bat","volt_arr","amp_bat"):
        vedata[varnames[i]]=float(vedata[varnames[i]])/1000
      for i in varnames:
        cn=varnames[i]
        if cn in vedata:
          channel_info[i]['meas_data'].append(vedata[cn])

    time.sleep(parameter['wait'])


# close the client
print("done")


