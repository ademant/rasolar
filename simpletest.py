# Simple demo of reading each analog input from the ADS1x15 and printing it to
# the screen.
# Author: Tony DiCola
# License: Public Domain
import time

# Import the ADS1x15 module.
#from ADS1x15 import ADS1015
from Adafruit_ADS1x15 import ADS1015


# Create an ADS1115 ADC (16-bit) instance.
adc = ADS1015()

# Or create an ADS1015 ADC (12-bit) instance.
#adc = ADS1015()

# Note you can change the I2C address from its default (0x48), and/or the I2C
# bus by passing in these optional parameters:
adcc = ADS1015(address=0x49, busnum=1)

# Choose a gain of 1 for reading voltages from 0 to 4.09V.
# Or pick a different gain to change the range of voltages that are read:
#  - 2/3 = +/-6.144V
#  -   1 = +/-4.096V
#  -   2 = +/-2.048V
#  -   4 = +/-1.024V
#  -   8 = +/-0.512V
#  -  16 = +/-0.256V
# See table 3 in the ADS1015/ADS1115 datasheet for more info on gain.
GAIN = 1

f1=open("/home/pi/adc.txt","w+")

# Print nice channel column headers.
# Main loop.
while True:
    # Read all the ADC channel values in a list.
    values = [0]*9
    for i in range(4):
        # Read the specified ADC channel using the previously set gain value.
        values[0] = time.time()
        values[1+i] = adc.read_adc(i, gain=GAIN)
        values[5+i] = adcc.read_adc(i, gain=GAIN)
        # Note you can also pass in an optional data_rate parameter that controls
        # the ADC conversion time (in samples/second). Each chip has a different
        # set of allowed data rate values, see datasheet Table 9 config register
        # DR bit values.
        #values[i] = adc.read_adc(i, gain=GAIN, data_rate=128)
        # Each value will be a 12 or 16 bit signed integer value depending on the
        # ADC (ADS1015 = 12-bit, ADS1115 = 16-bit).
    # Print the ADC values.
#    print("| {0:>6} | {1:>6} | {2:>6} | {3:>6} | {4:>6} | {5:>6} | {6:>6} | {7:>6} | {8:>6}".format(*values))
    f1.write("| {0:>6} | {1:>6} | {2:>6} | {3:>6} | {4:>6} | {5:>6} | {6:>6} | {7:>6} | {8:>6} | \n".format(*values))
    # Pause for half a second.
    time.sleep(0.5)
