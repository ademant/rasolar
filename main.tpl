<!DOCTYPE html>
<html>
<head>
<title>Übersicht {{server}}</title>
</head>
<body>
<p>Auslastung: {{cpupercent}}</p>
<p>bearbeitete Tickest: {{countticket}}</p>
<p>Ticket-Bearbeitungszeit: {{tickettime}}</p>
<p>Aktuelle Variablen:
<ul>
%for item in measdata:
%measlen=len(measdata[item]['measures'])
<li>{{measdata[item]['varname']}}: {{measlen}}</li>
%end
</ul>
<p><a href="/kfz">Dateneingabe KFZ</a></p>
<p><a href="/haus">Dateneingabe Haus</a></p>
<p> Sensoren:</p>
<form action="/" method="post">
<table>
<tr><th>Sensor</th><th>Aktiviert</th><th>in Betrieb</th></tr>
%for sns in sensors:
%snns=sensors[sns]
%sac="Inaktiv"
%sru=""
%if snns['enable']==1:
%sac="Aktiviert"
%if snns['running']:
%sru="läuft"
%else:
%sru='unterbrochen'
%end
%end
<tr><th>{{sns}}</th><th>{{sac}}</th><th>{{sru}}
%if sru=='unterbrochen':
<input name="sensor" value={{sns}} type="submit"/>
%end
</th></tr>
%end
</table>
</form>
</body>
</html>
