#!/usr/bin/env python

import numpy as np
import os, serial,time,socket,sys,json,logging
from meas_data import meas_data
# import the server implementation

pathname = os.path.dirname(sys.argv[0])        
abspath=os.path.abspath(pathname)

configfile=abspath+"/config.json"
cf=open(configfile,"r")
log_conf=json.load(cf)
cf.close()

parameter={"device":socket.gethostname(),"mean_count":5,"ring_length":10,"wait":0.5,"sigma":2,"cycle":10,"gpg_keyid":""}
for n in parameter:
  if n in log_conf:
    parameter[n]=log_conf[n]

bmcp9808=False
if "mcp9808" in log_conf:
  bmcp9808=True
  if "enable" in log_conf['mcp9808']:
    if log_conf['mcp9808']['enable'] == 0:
      bmcp9808=False
  if bmcp9808:
    import Adafruit_MCP9808.MCP9808 as MCP9808
    try:
      sens_9808 = MCP9808.MCP9808()
    except:
      bmcp9808 = False
    else:
      sens_9808.begin()
      mcp_sigma=parameter['sigma']
      if "sigma" in log_conf['mcp9808']:
        mcp_sigma=int(log_conf['mcp9808']['sigma'])
        if mcp_sigma < 1:
          mcp_sigma=1
      mcp_data=meas_data(var_name="temperature",multiplicator=1000,mean_count=parameter['cycle'],store_each_cycle=True,ring_length=parameter['ring_length'],device=parameter['device'],sensor="MCP9808",i2c=0,store_dir="/home/pi/log/",sigma=mcp_sigma,digits=4)
      if "sqlserver" in log_conf:
        hostname="localhost"
        port=8080
        mcp_data.set_sql(host=hostname,port=port)
      if "opensensemap" in log_conf['mcp9808']:
        osm=log_conf['mcp9808']['opensensemap']
        if "sensebox_id" in osm:
          sid=osm['sensebox_id']
          if "sensors" in osm:
            if "temperature" in osm['sensors']:
              mcp_data.set_sensebox(sid,osm['sensors']['temperature'])
      bmqtt=False
      if "mqtt" in log_conf:
        bmqtt=True
        lcmq=log_conf['mqtt']
        mbroker=""
        if 'broker' in lcmq:
          mbroker=lcmq['broker']
        else:
          bmqtt=False
        mport=1883
        if 'port' in lcmq:
          mport=lcmq['port']
        if bmqtt:
          mcp_data.set_mqtt(broker=mbroker,port=mport)


if bmcp9808:
  while True:
  
  # read bme280 (temperature, humidity, pressure)
    mcp_data.append(sens_9808.readTempC())
    time.sleep(parameter['wait'])


# close the client
print("done")


