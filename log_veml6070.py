#!/usr/bin/env python

import numpy as np
import os, serial,time,socket,sys,json
from meas_data import meas_data
# import the server implementation
# import ADC
pathname = os.path.dirname(sys.argv[0])        
abspath=os.path.abspath(pathname)

configfile=abspath+"/config.json"
cf=open(configfile,"r")
log_conf=json.load(cf)
cf.close()

parameter={"device":socket.gethostname(),"mean_count":5,"ring_length":10,"wait":0.5,"sigma":2,"cycle":10,"check_last":0,"gpg_keyid":""}
for n in parameter:
  if n in log_conf:
    parameter[n]=log_conf[n]

bveml=False
if "veml6070" in log_conf:
  bveml=True
  if "enable" in log_conf['veml6070']:
    if log_conf['veml6070']['enable'] == 0:
      bveml=False
  if bveml:
    import veml6070
    veml_port=1
    if "port" in log_conf['veml6070']:
      veml_port=int(log_conf['veml6070']['port'])
    try:
      veml = veml6070.Veml6070(i2c_bus=veml_port)
    except:
      bveml=False
    else:
      tsl_sigma=parameter['sigma']
      for n in parameter:
        if n in log_conf:
          parameter[n]=log_conf[n]
      if "sigma" in log_conf['veml6070']:
        tsl_sigma=int(log_conf['veml6070']['sigma'])
        if tsl_sigma < 1:
          tsl_sigma=1
      tsl_data=meas_data(var_name="uv",multiplicator=1000,mean_count=parameter['cycle'],store_each_cycle=True,ring_length=parameter['ring_length'],device=parameter['device'],sensor="VEML6070",i2c=veml_port,store_dir="/home/pi/log/",sigma=tsl_sigma,digits=4,check_last=parameter['check_last'])
      bmqtt=False
      if "mqtt" in log_conf:
        bmqtt=True
        lcmq=log_conf['mqtt']
        mbroker=""
        if 'broker' in lcmq:
          mbroker=lcmq['broker']
        else:
          bmqtt=False
        mport=1883
        if 'port' in lcmq:
          mport=lcmq['port']
        if bmqtt:
          tsl_data.set_mqtt(broker=mbroker,port=mport)
      if "sqlserver" in log_conf:
        hostname="localhost"
        port=8080
        tsl_data.set_sql(host=hostname,port=port)
      if "opensensemap" in log_conf['tsl2591']:
        osm=log_conf['veml6070']['opensensemap']
        if "sensebox_id" in osm:
          sid=osm['sensebox_id']
          if "sensors" in osm:
            if "uv" in osm['sensors']:
              tsl_data.set_sensebox(sid,osm['sensors']['uv'])


if bveml:
  while True:
  
  # read bme280 (temperature, humidity, pressure)
    veml.set_integration_time(veml6070.INTEGRATIONTIME_1T)
    tsl_data.append(veml.get_uva_light_intensity())
    time.sleep(parameter['wait'])


# close the client
print("done")
