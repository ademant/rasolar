def init(ads_conf):
  badc=True
  if "enable" in ads_conf:
    if ads_conf['enable'] == 0:
      badc=False
  if badc:
    adc={}
    GAIN=1
    if "gain" in ads_conf:
      GAIN=ads_conf['gain']
    if "adc" in ads_conf:
      from Adafruit_ADS1x15 import ADS1115
      from Adafruit_ADS1x15 import ADS1015
      adc_count=0
      tadc=ads_conf['adc']
      for x in tadc:
        y=tadc[x]
        adc_address=0x48
        if "address" in y:
          adc_address=int(y['address'],16)
        abc_bus=1
        if "busnum" in y:
          adc_bus=y['busnum']
        adc_type=1015
        if "type" in y:
          adc_type=y['type']
        adc_assigned=False
        if adc_type == 1115:
          try:
            adc[adc_count]=ADS1115(address=adc_address,busnum=adc_bus)
            adc_assigned=True
          except:
            print("could not assign ADC")
        if adc_type == 1015:
          try:
            adc[adc_count]=ADS1015(address=adc_address,busnum=adc_bus)
            adc_assigned=True
          except:
		    print("could not assign ADC")
        if adc_assigned:
          for j in range(4):
            channel_names.append("a"+str(adc_count)+"_"+str(j))
          adc_count=adc_count+1
    else:
      badc=False
  return (badc,adc)
