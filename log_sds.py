#!/usr/bin/env python3
# sds module: https://github.com/ikalchev/py-sds011
# apt-get remove python3-serial
# pip3 install pyserial
import numpy as np
from serial import Serial
import os, time,socket,sys,json,random,sds011
from meas_data import meas_data

sensor="sds011"

pathname = os.path.dirname(sys.argv[0])        
abspath=os.path.abspath(pathname)

configfile=abspath+"/config.json"
try:
	cf=open(configfile,"r")
except:
	cf=open(configfile+".template","r")

log_conf=json.load(cf)
cf.close()
idf=open(abspath+"/id.json","r")
idlist=json.load(idf)["id"]
sens_id={}
for ids in idlist:
	if "sensor" in idlist[ids]:
		if idlist[ids]["sensor"]==sensor:
			sens_id[ids]=idlist[ids]
idf.close()

parameter={"device":socket.gethostname(),"mean_count":2,"ring_length":30,"wait":0.1,"cycle":20,"check_last":2,"gpg_keyid":"","usbport":"/dev/serial/by-id/usb-1a86_USB2.0-Serial-if00-port0","meascount":5,"meassleep":30}
for n in parameter:
	if n in log_conf:
		parameter[n]=log_conf[n]

bsds=False
if sensor in log_conf:
	bsds=True
	for n in parameter:
		if n in log_conf[sensor]:
			parameter[n]=log_conf[sensor][n]
	try:
		sensor=sds011.SDS011(parameter['usbport'],use_query_mode=True)
	except:
		bsds=False
	else:
		bpm25=False
		bpm10=False
		for ids in sens_id:
			if sens_id[ids]['varname']=="pm25":
				bpm25=True
				pm25=meas_data(var_name="pm25",ring_length=parameter['ring_length'],device=parameter['device'],sensorid=sens_id[ids],store_dir="/home/pi/log/",digits=3,check_last=parameter['check_last'])
				pm25.set_sql(host="localhost",port=8080,min_wait=parameter['wait'])
			if sens_id[ids]['varname']=="pm10":
				bpm10=True
				pm10=meas_data(var_name="pm10",ring_length=parameter['ring_length'],device=parameter['device'],sensorid=sens_id[ids],store_dir="/home/pi/log/",digits=3,check_last=parameter['check_last'])
				pm10.set_sql(host="localhost",port=8080,min_wait=parameter['wait'])

		bmqtt=False
		if "mqtt" in log_conf:
			bmqtt=True
			lcmq=log_conf['mqtt']
			mbroker=""
			if 'broker' in lcmq:
				mbroker=lcmq['broker']
			else:
				bmqtt=False
			mport=1883
			if 'port' in lcmq:
				mport=lcmq['port']
			if bmqtt:
				pm25.set_mqtt(broker=mbroker,port=mport)
				pm10.set_mqtt(broker=mbroker,port=mport)

a = 2

if bsds:
	while True:
		sensor.sleep(sleep=False)
		for i in range(parameter['meascount']):
			(data25,data10)=sensor.query()
			pm25.append(data25)
			pm10.append(data10)
		sensor.sleep()
		time.sleep(60*parameter['meassleep'])


# close the client

print("done")


