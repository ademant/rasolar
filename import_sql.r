library(foreach)

indata<-read.table("/home/ademant/src/rasolar/data/data_433058.txt",fill=TRUE,stringsAsFactors=FALSE)

raw_dat=unlist(sapply(1:nrow(indata),function(i){return(strsplit(indata[i,],";")[[1]])}))
var_names=unique(sapply(1:length(raw_dat),function(i){return(head(strsplit(raw_dat[i],":")[[1]],1))}))

raw_row=as.data.frame(t(as.data.frame(rep(NA,length(var_names)))))
names(raw_row)<-var_names

intable<-foreach(i=1:nrow(indata),.combine=rbind)%do%{
	tid<-strsplit(indata[i,],";")[[1]]
	tout<-as.data.frame(t(sapply(tid,function(i){return(strsplit(i,":")[[1]])})))
	tout[,2]<-as.numeric(as.character(tout[,2]))
	tout[,1]<-as.character(tout[,1])
	names(tout)<-c("var","val")
	tout$time<-NA
	if("time"%in%tout$var){
		tout$time<-head(tout[tout$var=="time","val"],1)
		tout<-tout[tout$var!="time",]
	}
	return(tout)
}
solar_table<-rbind(solar_table,intable)
