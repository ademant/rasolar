#!/usr/bin/env python

import numpy as np
import os, serial,time,socket,sys,json,logging,requests
from meas_data import meas_data
# import the server implementation

pathname = os.path.dirname(sys.argv[0])        
abspath=os.path.abspath(pathname)

configfile=abspath+"/config.json"
cf=open(configfile,"r")
log_conf=json.load(cf)
cf.close()

parameter={"device":socket.gethostname(),"mean_count":5,"ring_length":10,"wait":0.5,"cycle":5,"digits":5,"check_last":0,"gpg_keyid":""}
for n in parameter:
  if n in log_conf:
    parameter[n]=log_conf[n]

bmqtt=False
if "mqtt" in log_conf:
  bmqtt=True
  lcmq=log_conf['mqtt']
  mbroker=""
  if 'broker' in lcmq:
    mbroker=lcmq['broker']
  else:
    bmqtt=False
  mport=1883
  if 'port' in lcmq:
    mport=lcmq['port']  

# config of bme280 sensor
# use pip install RPi.bme280 for library
meas_types=["temperature","humidity","pressure"]
bbme=False
if "bme280" in log_conf:
  bbme=True
  if "enable" in log_conf['bme280']:
    if log_conf['bme280']['enable'] == 0:
      bbme=False
  if bbme:
    import smbus2
    import bme280
    if "port" in log_conf['bme280']:
      bme_port=log_conf['bme280']['port']
    else:
      bme_port=1
    if "address" in log_conf['bme280']:
      bme_add=int(log_conf['bme280']['address'],16)
    else:
      bme_add=0x77
    try:
      bme_bus=smbus2.SMBus(bme_port)
    except:
      bbme=False
    else:
      calibration_params=bme280.load_calibration_params(bme_bus,bme_add)
      bme_sigma=3
      for n in parameter:
        if n in log_conf['bme280']:
          parameter[n]=log_conf['bme280'][n]
      if "sigma" in log_conf['bme280']:
        bme_sigma=int(log_conf['bme280']['sigma'])
        if bme_sigma < 1:
          bme_sigma=1
      bme_cycle=5
      if "cycle" in log_conf['bme280']:
        bme_cycle=int(log_conf['bme280']['cycle'])
      bme_temp=meas_data(var_name="temperature",multiplicator=1000,ring_length=parameter['ring_length'],device=parameter['device'],sensor="BME280",i2c=bme_add,store_dir="/home/pi/log/",sigma=bme_sigma,digits=3,check_last=parameter['check_last'])
      bme_hum=meas_data(var_name="humidity",multiplicator=1000,ring_length=parameter['ring_length'],device=parameter['device'],sensor="BME280",i2c=bme_add,store_dir="/home/pi/log/",sigma=bme_sigma,digits=3,check_last=parameter['check_last'])
      bme_press=meas_data(var_name="pressure",multiplicator=1000,ring_length=parameter['ring_length'],device=parameter['device'],sensor="BME280",i2c=bme_add,store_dir="/home/pi/log/",sigma=bme_sigma,digits=5,check_last=parameter['check_last'])
      if ("mqtt" in log_conf['bme280']) and bmqtt:
        bme_temp.set_mqtt(broker=mbroker,port=mport)
        bme_hum.set_mqtt(broker=mbroker,port=mport)
        bme_press.set_mqtt(broker=mbroker,port=mport)
      if "sqlserver" in log_conf:
        hostname="localhost"
#        if "host" in log_conf['sqlserver']:
#	      hostname=log_conf['sqlserver']['host']
        port=8080
#        if "port" in log_conf['sqlserver']:
#	      port=int(log_conf['sqlserver']['port'])
        bme_temp.set_sql(host=hostname,port=port)
        bme_hum.set_sql(host=hostname,port=port)
        bme_press.set_sql(host=hostname,port=port)
      if "opensensemap" in log_conf['bme280']:
        osm=log_conf['bme280']['opensensemap']
        if "sensebox_id" in osm:
          sid=osm['sensebox_id']
          if "sensors" in osm:
            if "temperature" in osm['sensors']:
              bme_temp.set_sensebox(sid,osm['sensors']['temperature'])
            if "humidity" in osm['sensors']:
              bme_hum.set_sensebox(sid,osm['sensors']['humidity'])
            if "pressure" in osm['sensors']:
              bme_press.set_sensebox(sid,osm['sensors']['pressure'])
          


if bbme:
  while True:
  
  # read bme280 (temperature, humidity, pressure)
    bme_data=bme280.sample(bme_bus,bme_add,calibration_params)
    bme_temp.append(bme_data.temperature)
    bme_hum.append(bme_data.humidity)
    bme_press.append(bme_data.pressure)
    time.sleep(parameter['wait'])


# close the client
print("done")


