import tsl2591

tsl = tsl2591.Tsl2591(i2c_bus=1,sensor_address=0x29)  # initialize
full, ir = tsl.get_full_luminosity()  # read raw values (full spectrum and ir spectrum)
lux = tsl.calculate_lux(full, ir)  # convert raw values to lux
print lux, full, ir
