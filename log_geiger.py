import time,json
from datetime import datetime
import RPi.GPIO as GPIO

configfile="config.json"
cf=open(configfile,"r")
log_conf=json.load(cf)
cf.close()

parameter={"device":socket.gethostname(),"mean_count":5,"ring_length":10,"wait":0.5,"sigma":2,"cycle":10}
for n in parameter:
  if n in log_conf:
    parameter[n]=log_conf[n]

def tube_impulse_callback(channel): # threaded callback -- falling edge detected
    global counter # make counter global to be able to increment it
    counter+=1
    print "ping"

bgeiger=False
if "geiger" in log_conf:
  bgeiger=True
  GPIO.setmode(GPIO.BOARD) # use RaspPi board layout pin numbering
  GPIO.setup(32, GPIO.IN, pull_up_down=GPIO.PUD_UP)
  counter = 0


  # when a falling edge is detected on port 12, regardless of whatever 
  # else is happening in the program, the tube_impulse_callback will be run
  GPIO.add_event_detect(32, GPIO.FALLING, callback=tube_impulse_callback)

if bgeiger:
	try:
		while True:
			currentMinute = datetime.now().minute
			while datetime.now().minute == currentMinute: # this minute..
				time.sleep(1) # .. wait while add_event_detect detects pulses
			print str(counter)+" cpm"
			counter=0 # reset counter
	except KeyboardInterrupt:
		GPIO.cleanup() # clean up GPIO on CTRL+C exit
	except:
		GPIO.cleanup() # clean up GPIO on normal exit

