#!/bin/sh
script=$(readlink -f $0)
scriptpath=$(dirname ${script})

cd $scriptpath
nohup python3 pyweb.py &

for i in $(find ${scriptpath} -name "log_*.py"); do
	nohup python3 $i &
done
