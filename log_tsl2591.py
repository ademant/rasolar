#!/usr/bin/env python

import numpy as np
import os, serial,time,socket,sys,json,logging
from meas_data import meas_data
# import the server implementation
pathname = os.path.dirname(sys.argv[0])        
abspath=os.path.abspath(pathname)

configfile=abspath+"/config.json"
cf=open(configfile,"r")
log_conf=json.load(cf)
cf.close()

parameter={"device":socket.gethostname(),"mean_count":5,"ring_length":10,"wait":0.5,"sigma":2,"cycle":10,"check_last":0,"gpg_keyid":""}
for n in parameter:
  if n in log_conf:
    parameter[n]=log_conf[n]

btls=False
if "tsl2591" in log_conf:
  btls=True
  if "enable" in log_conf['tsl2591']:
    if log_conf['tsl2591']['enable'] == 0:
      btls=False
  if btls:
# https://github.com/maxlklaxl/python-tsl2591
    import tsl2591
    tsl_port=1
    if "port" in log_conf['tsl2591']:
      tsl_port=int(log_conf['tsl2591']['port'])
    tsl_add=0x29
    if "address" in log_conf['tsl2591']:
      tsl_add=int(log_conf['tsl2591']['address'],16)
    try:
      tsl = tsl2591.Tsl2591(i2c_bus=tsl_port,sensor_address=tsl_add)  # initialize
    except:
      btls=False
    else:
      tsl_sigma=parameter['sigma']
      for n in parameter:
        if n in log_conf['tsl2591']:
          parameter[n]=log_conf['tsl2591'][n]
      if "sigma" in log_conf['tsl2591']:
        tsl_sigma=int(log_conf['tsl2591']['sigma'])
        if tsl_sigma < 1:
          tsl_sigma=1
      tsl_data=meas_data(var_name="lux",multiplicator=1000,mean_count=parameter['cycle'],store_each_cycle=True,ring_length=parameter['ring_length'],device=parameter['device'],sensor="TSL2591",i2c=tsl_add,store_dir="/home/pi/log/",sigma=tsl_sigma,digits=5,check_last=parameter['check_last'])
      bmqtt=False
      if "mqtt" in log_conf:
        bmqtt=True
        lcmq=log_conf['mqtt']
        mbroker=""
        if 'broker' in lcmq:
          mbroker=lcmq['broker']
        else:
          bmqtt=False
        mport=1883
        if 'port' in lcmq:
          mport=lcmq['port']
        if bmqtt:
          tsl_data.set_mqtt(broker=mbroker,port=mport)
      if "sqlserver" in log_conf:
        hostname="localhost"
        port=8080
        tsl_data.set_sql(host=hostname,port=port)
      if "opensensemap" in log_conf['tsl2591']:
        osm=log_conf['tsl2591']['opensensemap']
        if "sensebox_id" in osm:
          sid=osm['sensebox_id']
          if "sensors" in osm:
            if "lux" in osm['sensors']:
              tsl_data.set_sensebox(sid,osm['sensors']['lux'])


if btls:
  while True:
  
  # read bme280 (temperature, humidity, pressure)
    tsl_full,tsl_ir=tsl.get_full_luminosity()
    tsl_data.append(tsl.calculate_lux(tsl_full,tsl_ir))
    time.sleep(parameter['wait'])


# close the client
print("done")


