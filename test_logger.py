#!/usr/bin/env python3

import numpy as np
import os, time,socket,sys,json,random
from meas_data import meas_data

pathname = os.path.dirname(sys.argv[0])        
abspath=os.path.abspath(pathname)

configfile=abspath+"/config.json"
try:
	cf=open(configfile,"r")
except:
	cf=open(configfile+".template","r")

log_conf=json.load(cf)
cf.close()

parameter={"device":socket.gethostname(),"mean_count":5,"ring_length":300,"wait":0.01,"cycle":20,"check_last":5,"gpg_keyid":""}
for n in parameter:
  if n in log_conf:
    parameter[n]=log_conf[n]


tl1=meas_data(var_name="test1",ring_length=parameter['ring_length'],device=parameter['device'],sensor="CPU",store_dir="/home/ademant/src/rasolar2/data/",digits=4,check_last=parameter['check_last'])
tl1.set_sql(host="localhost",port=8080,min_wait=1)
#tl1.set_rsa("25A4CF79414F10FD")
tl2=meas_data(var_name="test2",ring_length=parameter['ring_length'],device=parameter['device'],sensor="CPU",store_dir="/home/ademant/src/rasolar2/data/",digits=4,check_last=parameter['check_last'])
tl2.set_sql(host="localhost",port=8080,min_wait=1)
#tl2.set_rsa("25A4CF79414F10FD")
tl3=meas_data(var_name="test3",ring_length=parameter['ring_length'],device=parameter['device'],sensor="CPU",store_dir="/home/ademant/src/rasolar2/data/",digits=4,check_last=parameter['check_last'])
tl3.set_sql(host="localhost",port=8080,min_wait=1)
#tl3.set_rsa("25A4CF79414F10FD")
tl4=meas_data(var_name="test4",ring_length=parameter['ring_length'],device=parameter['device'],sensor="CPU",store_dir="/home/ademant/src/rasolar2/data/",digits=4,check_last=parameter['check_last'])
tl4.set_sql(host="localhost",port=8080,min_wait=1)
tl4.set_rsa("25A4CF79414F10FD")
tl5=meas_data(var_name="test5",ring_length=parameter['ring_length'],device=parameter['device'],sensor="CPU",store_dir="/home/ademant/src/rasolar2/data/",digits=4,check_last=parameter['check_last'])
tl5.set_sql(host="localhost",port=8080,min_wait=1)
tl5.set_rsa("25A4CF79414F10FD")
tl6=meas_data(var_name="test6",ring_length=parameter['ring_length'],device=parameter['device'],sensor="CPU",store_dir="/home/ademant/src/rasolar2/data/",digits=4,check_last=parameter['check_last'])
tl6.set_sql(host="localhost",port=8080,min_wait=1)
tl6.set_rsa("25A4CF79414F10FD")
tl7=meas_data(var_name="test7",ring_length=parameter['ring_length'],device=parameter['device'],sensor="CPU",store_dir="/home/ademant/src/rasolar2/data/",digits=4,check_last=parameter['check_last'])
tl7.set_sql(host="localhost",port=8080,min_wait=1)
tl7.set_rsa("25A4CF79414F10FD")

#if "sqlserver" in log_conf:
#  hostname="banana"
#  if "host" in log_conf['sqlserver']:
#	  hostname=log_conf['sqlserver']['host']
#  port=24049
#  if "port" in log_conf['sqlserver']:
#	  port=int(log_conf['sqlserver']['port'])
#  cpu_meas.set_sql(host=hostname,port=port)

bmqtt=False
if "mqtt" in log_conf:
  bmqtt=True
  lcmq=log_conf['mqtt']
  mbroker=""
  if 'broker' in lcmq:
    mbroker=lcmq['broker']
  else:
    bmqtt=False
  mport=1883
  if 'port' in lcmq:
    mport=lcmq['port']
  if bmqtt:
    tl1.set_mqtt(broker=mbroker,port=mport)
    tl2.set_mqtt(broker=mbroker,port=mport)
    tl3.set_mqtt(broker=mbroker,port=mport)
    tl4.set_mqtt(broker=mbroker,port=mport)
    tl5.set_mqtt(broker=mbroker,port=mport)
    tl6.set_mqtt(broker=mbroker,port=mport)
    tl7.set_mqtt(broker=mbroker,port=mport)

a = 2


while True:
  # get cpu temperature
#  cpu_meas.append(int(open('/sys/class/thermal/thermal_zone0/temp').read()))
  tl1.append(int(random.random()*2**16))
  tl2.append(int(random.random()*2**16))
  tl3.append(int(random.random()*2**16))
  tl4.append(int(random.random()*2**16))
  tl5.append(int(random.random()*2**16))
  tl6.append(int(random.random()*2**16))
  tl7.append(int(random.random()*2**16))
  
  time.sleep(parameter['wait'])


# close the client

print("done")


