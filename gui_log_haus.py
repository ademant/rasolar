from Tkinter import *
import numpy as np
import os, time,socket,sys,json

payloads=["wasser","gas","kwh_haus","kwh_herd"]

def set_date():
	datafields["date"].delete(0,END)
	datafields["date"].insert(0,str(round(1000*time.time())))

def send_data():
	json_out={"device":socket.gethostname(),"payload":{}}
	for x in payloads:
		json_out["payload"][x]={"time":0,"sensor":"rechnung","value":0,"i2c":0}
	local_time=round(1000*time.time())
	try:
		local_time=int(datafields["date"].get())
	except:
		local_time=round(1000*time.time())
	for x in payloads:
		try:
			json_out["payload"][x]["value"]=int(datafields[x].get())
		except:
			del(json_out["payload"][x])
			print("kein ",x)
		else:
			json_out["payload"][x]["time"]=local_time
	if len(json_out["payload"])>0:
		try:
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		except:
			print("{}: could not connect to database".format(time.time()))
		else:
			try:
				s.connect(("banana", 24048))
			except:
				print("{}: could not connect to database".format(time.time()))
			else:
				s.sendall(json.dumps(json_out))
				s.close()

json_out={"device":socket.gethostname(),"payload":{}}
for x in payloads:
	json_out["payload"][x]={"time":0,"sensor":"rechnung","value":0,"i2c":0}

groot=Tk()
groot.wm_title("Hausverbrauch aufnehmen")
groot.config(background="#FFFFFF")
gframe=Frame(groot,width=400,height=300)
gframe.grid(row=0,column=0,padx=10,pady=3)

button_date=Button(gframe,text="Epoch",bg="#FF0000",width=15,command=set_date)
button_date.grid(row=0,column=0,padx=10,pady=3)
label_water=Label(gframe,text="Wasserzaehler")
label_water.grid(row=1,column=0,padx=10,pady=3)
label_gas=Label(gframe,text="Gaszaehler")
label_gas.grid(row=2,column=0,padx=10,pady=3)
label_electricity=Label(gframe,text="Stromzaehler")
label_electricity.grid(row=3,column=0,padx=10,pady=3)
label_oven=Label(gframe,text="Stromzaehler Herd")
label_oven.grid(row=4,column=0,padx=10,pady=3)

datafields={}
felder=["date","wasser","gas","kwh_haus","kwh_herd"]

for x in range(len(felder)):
	datafields[felder[x]]=Entry(gframe,width=18)
	datafields[felder[x]].grid(row=x,column=1,padx=10,pady=3)

button_send=Button(gframe,text="Absenden",bg="#00FF00",width=15,command=send_data)
button_send.grid(row=5,column=1,padx=10,pady=3)

datafields["date"].insert(0,str(round(1000*time.time())))

groot.mainloop()
