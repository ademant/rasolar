#!/usr/bin/env python

import numpy as np
import os, serial,time,socket,sys,json
from meas_data import meas_data
# import the server implementation

pathname = os.path.dirname(sys.argv[0])        
abspath=os.path.abspath(pathname)

configfile=abspath+"/config.json"
cf=open(configfile,"r")
log_conf=json.load(cf)
cf.close()

channel_names=[]
channel_info={}

parameter={"device":socket.gethostname(),"mean_count":5,"ring_length":10,"wait":0.5,"sigma":2,"cycle":10,"check_last":5,"gpg_keyid":""}
for n in parameter:
  if n in log_conf:
    parameter[n]=log_conf[n]


# declare adc
badc=False
GAIN=1
if "ads1x15" in log_conf:
  badc=True
  ads_conf=log_conf['ads1x15']
  if "enable" in ads_conf:
    for n in parameter:
      if n in log_conf['ads1x15']:
        parameter[n]=log_conf['ads1x15'][n]
    if ads_conf['enable'] == 0:
      badc=False
  bmqtt=False
  if "mqtt" in log_conf:
    bmqtt=True
    lcmq=log_conf['mqtt']
    mbroker=""
    if 'broker' in lcmq:
      mbroker=lcmq['broker']
    else:
      bmqtt=False
    mport=1883
    if 'port' in lcmq:
      mport=lcmq['port']  

  if "sqlserver" in log_conf:
    hostname="localhost"
    port=8080
  if badc:
    adc={}
    GAIN=1
    for n in parameter:
      if n in ads_conf:
        parameter[n]=ads_conf[n]
    if "gain" in ads_conf:
      GAIN=ads_conf['gain']
    if "adc" in ads_conf:
      tsl_sigma=parameter['sigma']
      if "sigma" in log_conf['ads1x15']:
        tsl_sigma=int(log_conf['ads1x15']['sigma'])
        if tsl_sigma < 1:
          tsl_sigma=1
      from Adafruit_ADS1x15 import ADS1115
      from Adafruit_ADS1x15 import ADS1015
      adc_count=0
      tadc=ads_conf['adc']
      for x in tadc:
        y=tadc[x]
        adc_address=0x48
        if "address" in y:
          adc_address=int(y['address'],16)
        abc_bus=1
        if "busnum" in y:
          adc_bus=y['busnum']
        adc_type=1015
        if "type" in y:
          adc_type=y['type']
        adc_assigned=False
        if adc_type == 1115:
          try:
            adc[adc_count]=ADS1115(address=adc_address,busnum=adc_bus)
            adc_assigned=True
          except:
            print("could not assign ADC")
        if adc_type == 1015:
          try:
            adc[adc_count]=ADS1015(address=adc_address,busnum=adc_bus)
            adc_assigned=True
          except:
            print("could not assign ADC")
        if adc_assigned:
          for j in range(4):
            cnadc="a"+str(adc_count)+"_"+str(j)
            cnadcs="a"+"_"+str(j)
            channel_names.append(cnadc)
            channel_info[cnadc]={"meas_data": meas_data(var_name=cnadcs,multiplicator=1,mean_count=parameter['cycle'],store_each_cycle=True,ring_length=parameter['ring_length'],device=parameter['device'],sensor="ads"+str(adc_type),i2c=adc_address,store_dir="/home/pi/log",sigma=tsl_sigma,check_last=parameter['check_last'])}
            if bmqtt and ("mqtt" in log_conf['ads1x15']):
              channel_info[cnadc]['meas_data'].set_mqtt(broker=mbroker,port=mport)
            if "sqlserver" in log_conf:
              channel_info[cnadc]['meas_data'].set_sql(host=hostname,port=port)
          adc_count=adc_count+1
    else:
      badc=False


if badc:
      
  while True:
  
    for i in adc:
      cname="a"+str(i)+"_{0}"
      for j in range(4):
        channel_info[cname.format(j)]['meas_data'].append(adc[i].read_adc(j,gain=GAIN))

    time.sleep(parameter['wait'])


# close the client
print("done")


